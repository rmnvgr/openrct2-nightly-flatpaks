#!/bin/bash

# This script will build a flatpak repository for the develop branch of OpenRCT2
# and store it in a S3 bucket.
#
# It needs the following environment variables:
# 	- AWS_ACCESS_KEY_ID: The AWS access key ID to use to authenticate.
# 	- AWS_SECRET_ACCESS_KEY: The AWS secret access key to use to authenticate.
# 	- AWS_DEFAULT_REGION: The AWS region to send the request to.
# 	- S3_BUCKET: The S3 bucket where the repo is stored.
# 	- S3_PREFIX (optional): The S3 bucket prefix to where the repo is stored.
# 	- GPG_PUBLIC_KEY: The GPG public key ID that will be used to sign the repo.
# 	- GPG_PRIVATE_KEY: The armored GPG private key that will be used to sign the
# 		repo. Replace the line returns with \n.

set -e

# Test environment variables
[[ -z ${AWS_ACCESS_KEY_ID} ]] && echo "Specify the AWS access key ID with the AWS_ACCESS_KEY_ID environment variable." && exit 1
[[ -z ${AWS_SECRET_ACCESS_KEY} ]] && echo "Specify the AWS secret access key with the AWS_SECRET_ACCESS_KEY environment variable." && exit 1
[[ -z ${AWS_DEFAULT_REGION} ]] && echo "Specify the S3 region with the S3_REGION environment variable." && exit 1
[[ -z ${S3_BUCKET} ]] && echo "Specify the S3 bucket with the S3_BUCKET environment variable." && exit 1
[[ -z ${GPG_PUBLIC_KEY} ]] && echo "Specify the GPG public key with the GPG_PUBLIC_KEY environment variable." && exit 1
[[ -z ${GPG_PRIVATE_KEY} ]] && echo "Specify the GPG public key with the GPG_PRIVATE_KEY environment variable." && exit 1

# Import GPG secret key for repository signing
echo -e "${GPG_PRIVATE_KEY}" > secret.key
gpg --batch --import secret.key

# Init the repo
ostree --repo=repo init --mode=archive-z2

# Sync the existing repo from S3
aws s3 sync s3://${S3_BUCKET}/${S3_PREFIX} repo

# Build the flatpak and export to the repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak-builder \
	--repo=repo \
	--gpg-sign="${GPG_PUBLIC_KEY}" \
	--from-git=https://github.com/flathub/io.openrct2.OpenRCT2.git \
	--from-git-branch=develop \
	--install-deps-from=flathub \
	--default-branch=develop \
	--delete-build-dirs \
	--force-clean \
	build \
	io.openrct2.OpenRCT2.yaml

# Update the repo
flatpak build-update-repo \
	--gpg-sign="$GPG_PUBLIC_KEY" \
	--title="OpenRCT2 development flatpaks" \
	--default-branch=develop \
	--generate-static-deltas \
	--prune \
	--prune-depth=7 \
	repo

# Sync back the repo to S3
aws s3 sync repo s3://${S3_BUCKET}/${S3_PREFIX} --delete
