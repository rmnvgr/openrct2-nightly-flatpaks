FROM fedora:latest

# Install dependencies
RUN dnf update -y
RUN dnf install -y gnupg2 awscli flatpak-builder
RUN dnf clean all

# Copy build script
COPY build-flatpak-repo.sh /usr/bin
RUN chmod a+x /usr/bin/build-flatpak-repo.sh

WORKDIR /var/local

CMD build-flatpak-repo.sh
